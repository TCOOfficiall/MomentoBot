package net.momentonetwork.momentobot.commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.Role;
import net.momentonetwork.momentobot.Main;
import net.momentonetwork.momentobot.command.CommandExecutor;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class RolesCommand extends CommandExecutor {

    public RolesCommand() {
        super("roles", "roles @role1 @role2 @role3 @etc");
    }

    @Override
    protected void run(Message msg, String[] args) {
        Guild g = msg.getGuild();

        ArrayList<String> prefixes = new ArrayList<>(Arrays.asList("Dutch", "English", "Alerts", "Skyblock"));
        ArrayList<Role> rolesToGive =
                msg.getMentionedRoles().stream()
                        .filter(r -> prefixes.contains(r.getName().split(":")[0].trim()))
                        .collect(Collectors.toCollection(ArrayList::new));

        if (rolesToGive.size() == 0){
            // todo show role help
            EmbedBuilder embed = new EmbedBuilder();
            embed.setAuthor(msg.getAuthor().getName(), null, msg.getAuthor().getAvatarUrl());
            embed.setDescription(Main.getSetting("cp") + "roles @ROL1\n\nThis command gives you one of the roles that is allowed to get by momento. These are:\n- @Dutch\n- @English");
            embed.setColor(Color.BLUE);
            msg.getChannel().sendMessage(embed.build()).queue();
            return;
        }
        g.getController().addRolesToMember(g.getMember(msg.getAuthor()), rolesToGive.toArray(new Role[rolesToGive.size()])).queue();
        msg.addReaction("✅").queue();
    }
}
