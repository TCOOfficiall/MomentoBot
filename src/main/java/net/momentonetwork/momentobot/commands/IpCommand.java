package net.momentonetwork.momentobot.commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.momentonetwork.momentobot.Main;
import net.momentonetwork.momentobot.command.CommandExecutor;

import java.awt.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class IpCommand extends CommandExecutor {

        Date date = new Date();
        Guild g;
                          public IpCommand() { super("ip");  }

    @Override
    protected void run(Message msg, String[] args) {
        msg.getChannel().sendMessage("You can join momentonetwork with the ip: ***mc.momentonetwork.net***").queue();

            EmbedBuilder embed = new EmbedBuilder();

            embed.setDescription("**User:** " + msg.getAuthor().getName() + "\n**Time:** " + date + "\n\n**Command:** "+msg.getContentRaw());
            embed.setColor(Color.ORANGE);
            msg.getGuild().getTextChannelById("435080429029294091").sendMessage(embed.build()).queue();

    }
}